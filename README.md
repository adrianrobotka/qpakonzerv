# QpaKonzerv

A QpaKonzerv egy konvertáló program. Célja hogy megkönnyítse a Schönherz Mátrix készítését.
Megfelelő méretre(32x26) vágott képekből QP4 formátumú fájlt készít.

## Képernyőkép
![képernyőkép](docs/screenshot.png)

## Letöltés
[Release build](https://gitlab.com/adrianrobotka/qpakonzerv/raw/master/QpaKonzerv/bin/Release/qpakonzerv.exe?inline=false)

## Köszönet
Köszönöm Karsa Zoltánnak, hogy összedobta ezt a programot. - Robotka Adrián

## Fejlesztés
Ha van kedved további funkciókat belerakni a QpaKonzerv programba, 
akkor dobj egy Merge Requestet vagy keress meg a robotka [kukac] sch.bme.hu címen.