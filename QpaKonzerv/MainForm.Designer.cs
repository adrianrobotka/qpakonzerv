namespace qpakonzerv
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addButton = new System.Windows.Forms.Button();
            this.generateButton = new System.Windows.Forms.Button();
            this.fileListBox = new System.Windows.Forms.ListBox();
            this.delayTextBox = new System.Windows.Forms.TextBox();
            this.selectedDeleteButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.moveUpButton = new System.Windows.Forms.Button();
            this.moveDownButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.filenameTextBox = new System.Windows.Forms.TextBox();
            this.previewPictureBox = new System.Windows.Forms.PictureBox();
            this.applyAllButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.previewPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(13, 5);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(143, 49);
            this.addButton.TabIndex = 0;
            this.addButton.Text = "Hozzáadás";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // generateButton
            // 
            this.generateButton.ForeColor = System.Drawing.Color.Green;
            this.generateButton.Location = new System.Drawing.Point(11, 376);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(145, 56);
            this.generateButton.TabIndex = 2;
            this.generateButton.Text = "Generálás";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // fileListBox
            // 
            this.fileListBox.FormattingEnabled = true;
            this.fileListBox.ItemHeight = 16;
            this.fileListBox.Location = new System.Drawing.Point(162, 44);
            this.fileListBox.Name = "fileListBox";
            this.fileListBox.Size = new System.Drawing.Size(626, 388);
            this.fileListBox.TabIndex = 3;
            this.fileListBox.SelectedIndexChanged += new System.EventHandler(this.fileListBox_SelectedIndexChanged);
            // 
            // delayTextBox
            // 
            this.delayTextBox.Location = new System.Drawing.Point(13, 320);
            this.delayTextBox.Name = "delayTextBox";
            this.delayTextBox.Size = new System.Drawing.Size(50, 22);
            this.delayTextBox.TabIndex = 4;
            this.delayTextBox.Text = "1000";
            this.delayTextBox.TextChanged += new System.EventHandler(this.delayTextBox_TextChanged);
            // 
            // selectedDeleteButton
            // 
            this.selectedDeleteButton.ForeColor = System.Drawing.Color.Red;
            this.selectedDeleteButton.Location = new System.Drawing.Point(12, 152);
            this.selectedDeleteButton.Name = "selectedDeleteButton";
            this.selectedDeleteButton.Size = new System.Drawing.Size(144, 43);
            this.selectedDeleteButton.TabIndex = 5;
            this.selectedDeleteButton.Text = "Kiválasztott törlése";
            this.selectedDeleteButton.UseVisualStyleBackColor = true;
            this.selectedDeleteButton.Click += new System.EventHandler(this.selectedDeleteButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.ForeColor = System.Drawing.Color.Red;
            this.clearButton.Location = new System.Drawing.Point(11, 201);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(145, 40);
            this.clearButton.TabIndex = 6;
            this.clearButton.Text = "Összes törlése";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // moveUpButton
            // 
            this.moveUpButton.Location = new System.Drawing.Point(10, 247);
            this.moveUpButton.Name = "moveUpButton";
            this.moveUpButton.Size = new System.Drawing.Size(146, 31);
            this.moveUpButton.TabIndex = 7;
            this.moveUpButton.Text = "Mozgatás FEL";
            this.moveUpButton.UseVisualStyleBackColor = true;
            this.moveUpButton.Click += new System.EventHandler(this.moveUpButton_Click);
            // 
            // moveDownButton
            // 
            this.moveDownButton.Location = new System.Drawing.Point(11, 284);
            this.moveDownButton.Name = "moveDownButton";
            this.moveDownButton.Size = new System.Drawing.Size(145, 30);
            this.moveDownButton.TabIndex = 8;
            this.moveDownButton.Text = "Mozgatás LE";
            this.moveDownButton.UseVisualStyleBackColor = true;
            this.moveDownButton.Click += new System.EventHandler(this.moveDownButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(159, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(548, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "A képek egymásutáni sorrendje, az itt megadott sorrendtől függ, fentről lefele ha" +
    "ladva";
            // 
            // filenameTextBox
            // 
            this.filenameTextBox.Location = new System.Drawing.Point(13, 348);
            this.filenameTextBox.Name = "filenameTextBox";
            this.filenameTextBox.Size = new System.Drawing.Size(143, 22);
            this.filenameTextBox.TabIndex = 10;
            this.filenameTextBox.Text = "out";
            // 
            // previewPictureBox
            // 
            this.previewPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.previewPictureBox.Location = new System.Drawing.Point(13, 60);
            this.previewPictureBox.Name = "previewPictureBox";
            this.previewPictureBox.Size = new System.Drawing.Size(143, 86);
            this.previewPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.previewPictureBox.TabIndex = 11;
            this.previewPictureBox.TabStop = false;
            // 
            // applyAllButton
            // 
            this.applyAllButton.Location = new System.Drawing.Point(70, 319);
            this.applyAllButton.Name = "applyAllButton";
            this.applyAllButton.Size = new System.Drawing.Size(86, 23);
            this.applyAllButton.TabIndex = 12;
            this.applyAllButton.Text = "Összesre";
            this.applyAllButton.UseVisualStyleBackColor = true;
            this.applyAllButton.Click += new System.EventHandler(this.applyAllButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 438);
            this.Controls.Add(this.applyAllButton);
            this.Controls.Add(this.previewPictureBox);
            this.Controls.Add(this.filenameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.moveDownButton);
            this.Controls.Add(this.moveUpButton);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.selectedDeleteButton);
            this.Controls.Add(this.delayTextBox);
            this.Controls.Add(this.fileListBox);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.addButton);
            this.MaximumSize = new System.Drawing.Size(818, 485);
            this.MinimumSize = new System.Drawing.Size(818, 485);
            this.Name = "MainForm";
            this.Text = "QpaKonzerv";
            ((System.ComponentModel.ISupportInitialize)(this.previewPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.ListBox fileListBox;
        private System.Windows.Forms.TextBox delayTextBox;
        private System.Windows.Forms.Button selectedDeleteButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button moveUpButton;
        private System.Windows.Forms.Button moveDownButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox filenameTextBox;
        private System.Windows.Forms.PictureBox previewPictureBox;
        private System.Windows.Forms.Button applyAllButton;
    }
}

