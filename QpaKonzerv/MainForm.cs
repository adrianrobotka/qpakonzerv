using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace qpakonzerv
{
    public partial class MainForm : Form
    {
        public string[] fn;
        public List<int> timingValues = new List<int>(); 

        public MainForm()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Multiselect = true;
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                fn = openFile.FileNames;
                for (int i = 0; i < fn.Length; i++)
                {
                    fileListBox.Items.Add(fn[i]);
                    timingValues.Add(int.Parse(delayTextBox.Text));
                }
            }
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            if (fileListBox.Items.Count != 0)
            {
                StreamWriter writer = new StreamWriter(filenameTextBox.Text + ".qp4");

                writer.WriteLine("-- __BITMAP -> qp4 - QpaKonzerv__\nmeta({\naudio = \"\",\nteam = \"\",\ntitle = \"\",\nyear = 2018})\nbeginclip(32,26,\"main\")\n");

                for (int i = 0; i < fn.Length; i++)
                {
                    writer.WriteLine(CreateImageFrame(fn[i], timingValues[i]));
                    writer.WriteLine();
                }
                
                writer.WriteLine("endclip()");
                writer.WriteLine("rootclip(\"main\")");
                writer.Close();
            }
            else MessageBox.Show("Nincsenek megnyitott képek!", "FIGYELEM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public string CreateImageFrame(string path, int ms)
        {
            string frame = "frame({\n";
            Bitmap bitmap = new Bitmap(path);
            for (int i = 0; i < bitmap.Height; i++)
            {
                for (int j = 0; j < bitmap.Width; j++)
                {
                    Color color = bitmap.GetPixel(j, i);
                    byte red = color.R;
                    byte green = color.G;
                    byte blue = color.B;
                    byte alpha = color.A;
                    if (red > 30 || green > 30 || blue > 30)
                        frame += "0x" + alpha.ToString("x2") + red.ToString("x2") + green.ToString("x2") + blue.ToString("x2") +  ",";
                    else frame += "0,";
                }
                frame += "\n";
            }
            return frame + "}," + ms + ")\n";
        }

        private void selectedDeleteButton_Click(object sender, EventArgs e)
        {
            fileListBox.Items.RemoveAt(fileListBox.SelectedIndex);
            timingValues.RemoveAt(fileListBox.SelectedIndex);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            fileListBox.Items.Clear();
            timingValues.Clear();
        }

        private void moveUpButton_Click(object sender, EventArgs e)
        {
            int k = fileListBox.SelectedIndex - 1  < 0 ? fileListBox.Items.Count - 1 : fileListBox.SelectedIndex - 1;
            string file = (string)fileListBox.SelectedItem;
            fileListBox.Items.RemoveAt(fileListBox.SelectedIndex);
            fileListBox.Items.Insert(k, file);
            fileListBox.SelectedIndex = k;
        }

        private void moveDownButton_Click(object sender, EventArgs e)
        {
            int k = fileListBox.SelectedIndex + 1 >= fileListBox.Items.Count ? 0 : fileListBox.SelectedIndex + 1;
            string file = (string)fileListBox.SelectedItem;
            fileListBox.Items.RemoveAt(fileListBox.SelectedIndex);
            fileListBox.Items.Insert(k, file);
            fileListBox.SelectedIndex = k;
        }

        private void fileListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            previewPictureBox.Image = new Bitmap((string) fileListBox.SelectedItem);
            delayTextBox.Text = timingValues[fileListBox.SelectedIndex].ToString();
        }

        private void delayTextBox_TextChanged(object sender, EventArgs e)
        {
            if(fileListBox.SelectedIndex != -1)
            {
                timingValues[fileListBox.SelectedIndex] = int.Parse(delayTextBox.Text);
            }
        }

        private void applyAllButton_Click(object sender, EventArgs e)
        {
            int k = int.Parse(delayTextBox.Text);
            for (int i = 0; i < timingValues.Count; i++)
            {
                timingValues[i] = k;
            }
        }
    }
}
